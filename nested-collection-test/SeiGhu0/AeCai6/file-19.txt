When I reflect upon the number of disagreeable people who I know who have gone
to a better world, I am moved to lead a different life.
		-- Mark Twain, "Pudd'nhead Wilson's Calendar"
